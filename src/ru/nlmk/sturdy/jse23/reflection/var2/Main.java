package ru.nlmk.sturdy.jse23.reflection.var2;

import java.lang.reflect.Field;

public class Main {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        Capucin mrAlex = new Capucin(10, 100, 33);
/*        Field field = mrAlex.getClass().getDeclaredField("footSize");
        System.out.println(field.get(mrAlex));
        field.set(mrAlex, 34);
        System.out.println(field.get(mrAlex));

        field = mrAlex.getClass().getDeclaredField("charming");
        field.setAccessible(true);
        System.out.println(field.get(mrAlex));
        field.set(mrAlex, 5);
        System.out.println(field.get(mrAlex));

        field = mrAlex.getClass().getDeclaredField("tailLength");
        field.setAccessible(true);
        System.out.println(field.get(mrAlex));
        field.set(mrAlex, 45);
        System.out.println(field.get(mrAlex));

        field = mrAlex.getClass().getField("numlegs");
        //field.setAccessible(true);
        System.out.println("numlegs = " + field.get(mrAlex));
        field.set(mrAlex, 45);
        System.out.println(field.get(mrAlex));

        field.set(mrAlex, 34);
        System.out.println(field.get(mrAlex));
*/
        Class<Capucin> clazz = (Class<Capucin>) mrAlex.getClass();
        ReflectionUtils.printClass(clazz);
    }
}
