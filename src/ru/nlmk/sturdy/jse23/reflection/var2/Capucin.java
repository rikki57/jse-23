package ru.nlmk.sturdy.jse23.reflection.var2;

public class Capucin extends Monkey{
    private final int tailLength = 40;
    private int charming;
    protected int harmful;
    public int footSize;

    public Capucin(int charming, int harmful, int footSize) {
        this.charming = charming;
        this.harmful = harmful;
        this.footSize = footSize;
    }

    protected void wash(Integer cout){
        System.out.println("Wash " + cout);
    }
}
