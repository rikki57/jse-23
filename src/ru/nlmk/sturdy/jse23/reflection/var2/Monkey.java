package ru.nlmk.sturdy.jse23.reflection.var2;

@MyAnnotation
public class Monkey {
    private final int numlegs = 2;
    public Integer age = 5;
    private String name;

    public String getName() {
        return name;
    }

    private void saySmth(String speech){
        System.out.println(speech);
    }
}
