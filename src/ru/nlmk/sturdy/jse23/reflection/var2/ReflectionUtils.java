package ru.nlmk.sturdy.jse23.reflection.var2;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;

public class ReflectionUtils {
    private ReflectionUtils() {
    }

    public static void printClass(Class clazz){
        System.out.println("");
        System.out.println("CLASS: " + clazz.getName());
        System.out.println("DECLARED FIELDS: ");
        printDeclaredFields(clazz);
        System.out.println("FIELDS: ");
        printFields(clazz);
        System.out.println("METHODS:");
        printMethods(clazz);
        System.out.println("ANNOTATIONS");
        printAnnotations(clazz);
        if (clazz.getSuperclass() != null){
            printClass(clazz.getSuperclass());
        }
    }

    private static void printAnnotations(Class clazz) {
        for (Annotation annotation : clazz.getAnnotations()){
            System.out.print("MethodName: " + annotation.annotationType().getName());
            System.out.println("");
        }
    }

    private static void printMethods(Class clazz) {
        for (Method method : clazz.getDeclaredMethods()){
            System.out.print("MethodName: " + method.getName());
            System.out.print(" Return type: " + method.getReturnType().getName());
            System.out.print(" Parameters: ");
            if (method.getParameterTypes().length > 0){
                for (Class parameterClass : method.getParameterTypes()){
                    System.out.print(parameterClass.getName() + ", ");
                }
            }
            System.out.println("");
        }
    }

    private static void printFields(Class clazz) {
        fieldIterator(clazz.getFields());
    }

    private static void printDeclaredFields(Class clazz) {
        fieldIterator(clazz.getDeclaredFields());
    }

    private static void fieldIterator(Field[] fields) {
        for (Field field : fields){
            System.out.print("name = " + field.getName());
            System.out.print(" type = " + field.getType());
            System.out.println(" modifier = " + field.getModifiers());
        }
    }
}
