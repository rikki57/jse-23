package ru.nlmk.sturdy.jse23.reflection.var1;

@TestAnnotation(name = "SomeName", aliases = {"SomeName1", "SomeName2"})
public class TestEntity {
    @TestAnnotation(name = "SomeName", aliases = {"SomeName1", "SomeName2"})
    String field;

    @TestAnnotation(name = "SomeName", aliases = {"SomeName1", "SomeName2"})
    String someMethod(){
        return "";
    }
}
