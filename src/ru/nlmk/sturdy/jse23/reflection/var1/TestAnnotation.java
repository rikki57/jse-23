package ru.nlmk.sturdy.jse23.reflection.var1;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE, ElementType.FIELD})
public @interface TestAnnotation {
    String name();
    int minArgs() default 0;
    int maxArgs() default Integer.MAX_VALUE;
    String[] aliases();
}
