package ru.nlmk.sturdy.jse23.reflection.var3;

public class Main {
    public static void main(String[] args) {
        MessageHandler messageHandler = new MessageHandler();
        messageHandler.handleCommand("hello", new Object[]{new Person("Alex", "Moscow")});
        messageHandler.handleCommand("help", new Object[]{"Sdfsdf"});
    }
}
