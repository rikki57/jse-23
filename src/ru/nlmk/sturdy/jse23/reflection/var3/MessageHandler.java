package ru.nlmk.sturdy.jse23.reflection.var3;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MessageHandler {
    private static final Map<String, Method> commands = new HashMap<>();
    private static final Commands commandStore = new Commands();

    static {
        for (Method method : commandStore.getClass().getDeclaredMethods()){
            if (method.isAnnotationPresent(Command.class)){
                Command command = method.getAnnotation(Command.class);
                commands.put(command.name(), method);
                for (String alias : command.aliases()){
                    commands.put(alias, method);
                }
            }
        }
    }

    public void handleCommand(String command, Object[] args){
        try{
            Method method = commands.get(command);
            if (method == null){
                return;
            }
            Command commandAnnotation = method.getAnnotation(Command.class);
            if (args.length < commandAnnotation.minArgs()){
                System.out.println("Too few arguments");
                return;
            } else if (args.length > commandAnnotation.maxArgs()){
                System.out.println("Too many arguments");
                return;
            }
            method.invoke(commandStore, (Object)args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }
}
