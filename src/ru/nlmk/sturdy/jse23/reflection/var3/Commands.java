package ru.nlmk.sturdy.jse23.reflection.var3;

import java.lang.reflect.Method;

public class Commands {
    @Command(name = "hello",
            minArgs = 1,
            maxArgs = 1,
            args = "Person person{name, city}",
            desc = "Say hello",
            showInHelp = true,
            aliases = "hi")
    public void hello(Object[] args){
        Person person = (Person) args[0];
        System.out.println("Hello, " + person.getName() + " are you from " + person.getCity());
    }

    @Command(name = "bye",
            args = "",
            desc = "Say bye",
            showInHelp = true,
            aliases = "goodbye")
    public void bye(String[] args){
        System.out.println("Bye");
    }

    @Command(name = "help",
            args = "",
            desc = "Show help",
            showInHelp = true,
            aliases = "помощь")
    public void help(Object[] args){
        StringBuilder stringBuilder = new StringBuilder("Список команд: \n");
        for (Method method : this.getClass().getMethods()){
            if (method.isAnnotationPresent(Command.class)){
                Command command = method.getAnnotation(Command.class);
                if (command.showInHelp()){
                    stringBuilder.append("bot, ").append(" ").append(command.args()).append(" - ")
                            .append(command.desc()).append("\n");
                }
            }
        }
        System.out.println(stringBuilder.toString());
    }
}
